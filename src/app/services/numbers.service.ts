import { Injectable } from "@angular/core"
import { PrimeDefinition } from "./prime.definition.interface"


@Injectable({
  providedIn: "root",
})
export class NumbersService {

  public numbers: PrimeDefinition[] = []

  constructor() {
  }


  generatePrimes({
                   originalStarter,
                   starter,
                   direction = "up",
                   count = 10,
                 }: { originalStarter: number, starter: number, direction?: "up" | "down", count?: number }): PrimeDefinition[] {
    const primes: PrimeDefinition[] = []
    let i = starter
    do {
      if (NumbersService.isPrime(i)) {
        primes.push({
          prime: i,
          offset: i - originalStarter,
          percentage: i / starter,
        })
      }
      if (direction === "up") {
        i = i + 1
      }
      else {
        i = i - 1
      }
    }
    while (primes.length < count && i > 0)

    if (direction === "down") {
      return primes.reverse()
    }
    else {
      return primes
    }
  }


  /**
   * @see https://en.wikipedia.org/wiki/Primality_test#JavaScript
   * @param num
   * @private
   */
  private static isPrime(num: number): boolean {
    if (num == 2 || num == 3) {
      return true
    }
    if (num <= 1 || num % 2 == 0 || num % 3 == 0) {
      return false
    }
    for (let i = 5; i * i <= num; i += 6) {
      if (num % i == 0 || num % (i + 2) == 0) {
        return false
      }
    }
    return true
  }
}
