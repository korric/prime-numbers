import { NumbersService } from './numbers.service'

describe('NumbersService', () => {
  let service: NumbersService;

  beforeEach(() => {
    service = new NumbersService();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should generatePrimes', () => {
    const primes = service.generatePrimes({ starter: 10 })
    expect(primes.length).toBe(10)
    expect(primes[0]).toBe(11)
    expect(primes[1]).toBe(13)
    expect(primes[2]).toBe(17)
    expect(primes[3]).toBe(19)
    expect(primes[4]).toBe(23)
    expect(primes[5]).toBe(29)
    expect(primes[6]).toBe(31)
    expect(primes[7]).toBe(37)
    expect(primes[8]).toBe(41)
    expect(primes[9]).toBe(43)
    console.log(primes)
  })

  it('should generatePrimes in descending order', () => {
    const primes = service.generatePrimes({ starter: 44, direction: 'down' })
    expect(primes.length).toBe(10)
    expect(primes[9]).toBe(43)
    expect(primes[8]).toBe(41)
    expect(primes[7]).toBe(37)
    expect(primes[6]).toBe(31)
    expect(primes[5]).toBe(29)
    expect(primes[4]).toBe(23)
    expect(primes[3]).toBe(19)
    expect(primes[2]).toBe(17)
    expect(primes[1]).toBe(13)
    expect(primes[0]).toBe(11)
    console.log(primes)
  })
});
