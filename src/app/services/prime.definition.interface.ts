export interface PrimeDefinition  {
  prime: number
  offset: number
  percentage: number
}