import { Component, OnInit } from '@angular/core'
import { NumbersService } from './services/numbers.service'
import { FormBuilder } from '@angular/forms'
import { animate, style, transition, trigger } from '@angular/animations'
import { PrimeDefinition } from "./services/prime.definition.interface"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('items', [
      transition(':enter', [
        style({ transform: 'scale(0.5)', opacity: 0 }),  // initial
        animate('1s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({ transform: 'scale(1)', opacity: 1 }))  // final
      ])
    ])
  ]
})
export class AppComponent implements OnInit {

  originalStarter = 0

  IMPLICIT_LIMIT = 100000000

  inputForm = this.formBuilder.group({
    starter: ''
  })

  constructor(public numbersService: NumbersService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
  }

  onImplicitSubmit() {
    const value = this.getStarterValue()
    if (value === false) {
      this.numbersService.numbers = []
    } else if (value && value < this.IMPLICIT_LIMIT) {
      this.originalStarter = value
      this.numbersService.numbers = this.numbersService.generatePrimes({ starter: value , originalStarter: value })
    }
  }

  onSubmit() {
    const value = this.getStarterValue()
    if (value === false) {
      this.numbersService.numbers = []
    } else {
      const starter = this.originalStarter = Number(this.inputForm.value.starter)
      this.numbersService.numbers = this.numbersService.generatePrimes({ starter: starter, originalStarter: starter})
    }
  }

  getStarterValue(): number | false {
    if (this.inputForm.value.starter == '') {
      return false
    } else {
      return Number(this.inputForm.value.starter)
    }
  }

  onScrollUp(event: any) {
    console.log(event)
    const newNumbers = this.numbersService.generatePrimes({
      originalStarter: this.originalStarter,
      starter: this.numbersService.numbers[0].prime - 1,
      direction: 'down',
      count: 3
    })

    this.numbersService.numbers = [...newNumbers, ...this.numbersService.numbers]
  }

  onScrollDown(event: any) {
    console.log(event)
    const newNumbers = this.numbersService.generatePrimes({
      originalStarter: this.originalStarter,
      starter: this.numbersService.numbers[this.numbersService.numbers.length - 1].prime + 1,
      direction: 'up',
      count: 3
    })
    this.numbersService.numbers = [...this.numbersService.numbers, ...newNumbers]
  }

  getStarterNumber() {
    return Number(this.inputForm.value.starter)
  }
}
