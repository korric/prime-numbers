import { Component, Input, OnInit } from "@angular/core"
import { PrimeDefinition } from "../services/prime.definition.interface"

@Component({
  selector: 'component-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.scss']
})
export class NumberComponent implements OnInit {

  prime!: number
  details = ''

  constructor() { }

  @Input() input!: PrimeDefinition

  ngOnInit(): void {
    this.prime = this.input.prime

    let offset = ''
    if (this.input.offset < 0) {
      offset = `${this.input.offset}`
    } else if (this.input.offset > 0) (
      offset = `+${this.input.offset}`
    )

    let percentage = ''
    if (this.input.percentage < 1) {
     percentage = `-${((1-this.input.percentage) * 100).toFixed(0)}%`
    } else if (this.input.percentage > 1) {
      percentage = `+${((this.input.percentage-1) * 100).toFixed(0)}%`
    }

    if (offset || percentage) {
     this.details = `${[offset, percentage].join('; ')}`
    }
  }

}
