# PrimeNumbers

A simple prime number generator. Build mainly to learn and experiment with Angular. 

On the page enter a number and the program will generate a list of prime numbers after the number. 
The first number is clickable and generates lower numbers. The last number is clickable and generates higher numbers.

The numbers will be generated as you typu up to 10000, after that you must submit the input box with using enter.

Beware - the prime number generator is not optimized, so entering very large numbers may freeze and crash the page.

The page is visible on the page: https://primes.korinek.link using Gitlab Pages.

## Motivation

As a proper weirdo when paying in pubs I like to round the total price including tips to the nearest prime number. 
Most of the waiters and waitresses are seriously confused by that. 
However, I cannot count primes that fast, so I have built this little program to help me.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
